package com.prudential.prumobile;

import android.content.Intent;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

/**
 * Created by andrewfong on 8/1/2019.
 */

public class WebviewModule extends ReactContextBaseJavaModule {
    public WebviewModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "PruWebViewManager";
    }

    @ReactMethod
    public void showWebview(){
        Intent intent = new Intent(this.getReactApplicationContext(),WebviewActivity.class);
        this.getReactApplicationContext().startActivity(intent);
    }
}
