export const CHANGE_LANG = 'App/CHANGE_LANG';
export const ROOT_CHANGED = 'App/ROOT_CHANGED';
export const GET_PUBLICKEY_SUCCESS = 'App/GET_PUBLICKEY_SUCCESS';
export const E2EE_READY = 'App/E2EE_READY';
export const E2EE_TEST = 'E2EE_TEST';
export const ERROR_BANNER = 'App/ERROR_BANNER';
export const ERROR_BANNER_CLEAR = 'App/ERROR_BANNER_CLEAR';

export const GET_CONFIG_SUCCESS = 'App/GET_CONFIG_SUCCESS';
export const GET_FAQ_SUCCESS = 'App/GET_FAQ_SUCCESS';
export const CLEAR_FAQ = 'App/CLEAR_FAQ';