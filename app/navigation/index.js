import { Navigation } from 'react-native-navigation'

export const leftMenu = {
      	id:'Drawer',
        component:{
      	name: 'pruMobile.Drawer',
        passProps: {
          text: 'This is a left side menu screen'
        }
      }
}

export const goToHome = () => Navigation.setRoot({
	root:{
      sideMenu: {
      	left:leftMenu,
        center:{
          stack: {
          	id:'Home',
            options:{},
            children:[
              {
              	component: {
              	name: 'pruMobile.Welcome.Home',
                passProps: {
                  text: 'stack with one child'
                }
              }
            }]
          }
        }
      }
  	}
})