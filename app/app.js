/** 
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { registerScreens } from './screens';
import { Navigation } from 'react-native-navigation';
import {goToHome} from './navigation'


const store = configureStore();

registerScreens(store, Provider);

type Props = {};

// Navigation.events().registerAppLaunchedListener(() => {
//   Navigation.setRoot({
//     root: {
//       component: {
//         name: "navigation.playground.WelcomeScreen"
//       }
//     }
//   });
// });

class App extends Component{
  constructor(props) {
    super(props);
    store.subscribe(this.onStoreUpdate.bind(this));
    // store.dispatch(appActions.appInitialized());
      Navigation.events().registerAppLaunchedListener(() => {
        this.startApp("welcome");
      })
    
  }

  onStoreUpdate(){
    console.log("#####onStoreUpdate")
    let {root} = store.getState().app;
     
      // if (this.currentRoot != root) {
      //   this.currentRoot = root;
      //   Navigation.events().registerAppLaunchedListener(() => {
      //     this.startApp(root);
      //   })
      // }
  }

  startApp(root){
    switch(root){
      case "welcome":
      goToHome();
      break;
      default:
      goToHome();
    }
  }
}



export default App
