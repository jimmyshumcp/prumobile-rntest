import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View,NativeModules,Button,Alert} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../../actions';
import MapView from '../../components/Map';
import {Navigation} from 'react-native-navigation'
import styles from '../../styles/common'
import Config from 'react-native-config'

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

var TouchIdManager = NativeModules.TouchIdManager
var WebViewManager = NativeModules.PruWebViewManager

class Welcome extends Component {
	constructor(props){
		super(props);
		this.goToPageTwo = this.goToPageTwo.bind(this);
	}
	componentDidMount(){
		// TouchIdManager.startTouchID();
	}
	async startTouchID(){
		// TouchIdManager.startTouchID();
		try{
			let result = await TouchIdManager.startTouchIDPromise();
			Alert.alert("Auth success","Auth success");
		} catch(err){
			console.log("####",err);
			Alert.alert("Auth error",err.message);
		}
	}
	showWebview(){
		WebViewManager.showWebview();
	}
	goToPageTwo(){
		Navigation.push(this.props.componentId,{
			component:{
				name:"pruMobile.Welcome.PageTwo"
			}
		})
	}
	render() {
    return <View style={styles.container}>
    	<Text>{Config.ENV_NAME}</Text>
    	<Button title="Show Web View" onPress={this.showWebview}></Button>
    	<Button title="Show Touch ID" onPress={this.startTouchID}></Button>
    	<Button title="Next Page" onPress={this.goToPageTwo}></Button>
    	</View>
    // return <MapView style={{ flex: 1 }} />;
  }
}

function mapStateToProps(state) {
  return {
    app: state.app
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(Welcome);