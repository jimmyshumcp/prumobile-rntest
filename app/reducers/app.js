
import * as types from '../actions/types';

import Immutable from 'seamless-immutable';

const initialState = Immutable({
  	root: undefined,// 'login' / 'after-login'
  	lang:'en',
	publicKey: undefined,
	e2ee: '',
	e2eeReady: false,
	config:{},
	appName: '',
	appError: '',
	faq:{},
});

const appReducer = {
	app: (state:State = initialState, action:Action)=>{
		switch (action.type) {
		    case types.ROOT_CHANGED:
		      // return state.merge({
					// 	root: action.root
					// });
					state = {...state, root:action.root};
					return state;
		    case types.CHANGE_LANG:
		    	state = {...state, lang: action.data};
		    	return state;
		    case types.GET_PUBLICKEY_SUCCESS:
		    	state = {...state, publicKey: action.data};
					return state;
					
				case types.E2EE_READY:
					state = {...state, e2ee:action.data, e2eeReady: true };
					return state;

				case types.E2EE_TEST:
					state = {...state};
					return state;

				case types.GET_CONFIG_SUCCESS:
					state = {...state, config: action.data, appName: action.data.appName};
					return state;

				case types.ERROR_BANNER:
					state = {...state, appError: action.data};
					return state;

				case types.ERROR_BANNER_CLEAR:
					state = {...state, appError: ''};
					return state;

				case types.LOGOUT_SUCCESS:	
					state = {...state, 
						root: undefined,// 'login' / 'after-login'
						lang:'en',
						publicKey: undefined,
						e2ee: '',
						config:{},
						appName: '',
						appError: ''
					};
					return state;
				case types.GET_FAQ_SUCCESS:
					state = {...state, faq: action.data};
					return state;
				case types.CLEAR_FAQ:
					state = {...state, faq: {}};
					return state;
		    default:
		      return state;
		  }
	}
}

export default appReducer;
