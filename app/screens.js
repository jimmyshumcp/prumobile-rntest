import { Navigation } from 'react-native-navigation';

import WelcomeScreen from './containers/Welcome/Welcome';
import PageTwo from './containers/Welcome/PageTwo';
import Drawer from './containers/Drawer/Drawer';


export function registerScreens(store,provider){
	Navigation.registerComponentWithRedux("pruMobile.Welcome.Home", () => WelcomeScreen,provider,store);
	Navigation.registerComponentWithRedux("pruMobile.Welcome.PageTwo", () => PageTwo,provider,store);
	Navigation.registerComponentWithRedux("pruMobile.Drawer", () => Drawer,provider,store);

}


