/** @format */

import { Provider } from 'react-redux';
import configureStore from './app/store/configureStore';
import { registerScreens } from './app/screens';
import { Navigation } from 'react-native-navigation';
import {goToHome} from './app/navigation'


const store = configureStore();

registerScreens(store, Provider);

Navigation.events().registerAppLaunchedListener(() => {

goToHome();

});
